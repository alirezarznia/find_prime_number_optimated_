//include <god>
/*                   ___    ____    ___    ___
      /\     |        |    |____|  |         /     /\
     /__\    |        |    |  \    |---     /     /__\
    /    \   |___    _|_   |   \   |___    /__   /    \
*/
#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 	        	printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(ll J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	printf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<long long,pii>
#define 	pdd         		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Testout 	        freopen("a.out","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).resize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
#define 	MAXN 		        100000+99
#define 	EPS 		        1e-15
#define 	inf 		        1ll<<62
#define     SF                  scanf
#define     PF                  printf
#define     F                   first
#define     S                   second
#define     vll                   vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);


typedef long long ll;
typedef long double ld;

using namespace std;

/* int dx[] = {1,-1,0,0} , dy[] = {0,0,1,-1}; */ // 4 Direction
/* int dx[] = {1,-1,0,0,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1,1,-1}; */ // 8 Direction
/* int dx[] = {1,-1,1,-1,2,2,-2,-2} , dy[] = {2,2,-2,-2,1,-1,1,-1}; */ // Knight Direction
/* int dx[] = {2,-2,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1}; */ // Hexagonal Direction

ll pw(ll x, ll y){
    if(y==0) return 1;
    ll p = pw(x , y/2);
    if(y%2) return x*p*p;
    return p*p;
}

inline ll get_num(char ch){
	if(ch == '-')return 0;
	else if(ch >= 'a' && ch <= 'z'){
		return 1 + (ch-'a');
	}
	else if(ch >= 'A' && ch <= 'Z'){
		return 27 +(ch - 'A');
	}
	else if(ch >= '0' && ch <= '9'){
		return 53 +(ch - '0');
	}
}


inline int GA(ll n , vector<ll>&vec){
    Rep(i ,n){ll x ;cin>>x ; vec.push_back(x);}
}
inline int GAEG(ll m ,vector<ll>vec[]){
    Rep(i ,m){ll x  , y ;cin>>x>>y ; x-- , y-- ; vec[x].push_back(y); vec[y].push_back(x);}
}
vector<pii> FF(ll x){
    vector<pii> v;
    if(!(x%2))
    {
        ll cnt = 0;
        while(!(x%2)) x/=2 , cnt++;
        v.push_back(MP(2ll ,cnt));
    }
    ll ss = sqrt(x);
    For(i ,3, ss+1){
        ll cnt = 0;
        if(!(x%i)){
        while(!(x%i)) x/=i , cnt++;
        v.push_back(MP(i ,cnt));
        }
    }
    return v;
}
int main(){
  //  Test;
    ll n , k;
    cin>>n>>k;
    vector<pii> v = FF(k);
    ll ans =inf;
    Rep(i ,v.size()){
        ll x = v[i].first , y = v[i].second;
        ll q = n;
        ll p = 0;
        while(q)     q/=x  , p+=q;
        ans = min(ans , p/y);
    }
    cout<<ans<<endl;
}
